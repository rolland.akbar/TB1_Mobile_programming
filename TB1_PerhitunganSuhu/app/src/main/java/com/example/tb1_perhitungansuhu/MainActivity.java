package com.example.tb1_perhitungansuhu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText celcius, kelvin, fahrenheit, reamur;
    Button btnConvert, btnReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        celcius = (EditText)findViewById(R.id.celciusField);
        kelvin = (EditText)findViewById(R.id.kelvinField);
        fahrenheit = (EditText)findViewById(R.id.fahrenheitField);
        reamur = (EditText)findViewById(R.id.reamurField);
        btnConvert = (Button)findViewById(R.id.buttonSubmit);
        btnReset = (Button)findViewById(R.id.buttonReset);
    }

    public void hitunganSuhu (View view){
        try{
            double celciusHitung =  Double.parseDouble(celcius.getText().toString());
            double kelvinHitung = celciusHitung+273.15;
            double fahrenheitHitung = (celciusHitung*1.8)+32;
            double reamurHitung = celciusHitung*0.8;
            kelvin.setText(String.valueOf(kelvinHitung));
            fahrenheit.setText(String.valueOf(fahrenheitHitung));
            reamur.setText(String.valueOf(reamurHitung));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void resetHitunganSuhu (View view){
        int setZero = 0;
        celcius.setText(String.valueOf(setZero));
        kelvin.setText(String.valueOf(setZero));
        fahrenheit.setText(String.valueOf(setZero));
        reamur.setText(String.valueOf(setZero));
    }
}